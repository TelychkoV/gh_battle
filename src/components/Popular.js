import React, {Component, Fragment} from 'react';
import {fetchPopularRepos} from '../utils/api';
import SelectedLanguages from './SelectedLanguages';
import PopularRepos from './PopularRepos';

export default class Popular extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedLanguage: 'All',
            repos: null,
            error: null
        }
        this.updateLanguage = this.updateLanguage.bind(this);
    }

    componentDidMount() {
        this.fetchPopularReposHandler();
    }

    fetchPopularReposHandler() {
        fetchPopularRepos(this.state.selectedLanguage)
            .then(response => this.setState({repos: response}))
            .catch(error => this.setState({error}));
    }

    updateLanguage(language) {
        this.setState({selectedLanguage: language, repos: null}, () => {
            this.fetchPopularReposHandler();
        })
    }

    render() {
        const {error, selectedLanguage, repos} = this.state;

        if(error) {
            return <h2>{error}</h2>
        }

        return (
            <Fragment>
                <SelectedLanguages
                    selectedLanguage={selectedLanguage}
                    onSelect={this.updateLanguage}
                />
                {repos ?
                    <PopularRepos repos={repos} /> :
                    <p style={{textAlign: 'center'}}>Loading ...</p>}
            </Fragment>
        )
    }
}