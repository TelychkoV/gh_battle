import React, {Component} from 'react';
import {Link} from "react-router-dom";
import PlayerInput from "./PlayerInput";
import PlayerPreview from "./PlayerPreview";

export default class Battle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            playerOneName: '',
            playerTwoName: '',
            playerOneImage: null,
            playerTwoImage: null,
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    updatePlayerState(id, data) {
        this.setState(() => {
            const newState = {};
            newState[id + 'Name'] = data;
            newState[id + 'Image'] = data ? 'https://github.com/' + data + '.png?size=200' : null;
            return newState;
        })
    }

    handleReset(id) {
        this.updatePlayerState(id, null);
    }

    handleSubmit(id, username) {
        this.updatePlayerState(id, username);
    }

    render() {
        const {playerOneName, playerTwoName, playerOneImage, playerTwoImage} = this.state;
        return (
            <div>
                <div className="row">
                    {!playerOneName &&
                        <PlayerInput
                            id='playerOne'
                            label='Player One'
                            onSubmit={this.handleSubmit}
                        />}
                    {playerOneImage &&
                        <PlayerPreview
                            avatar={playerOneImage}
                            username={playerOneName}>
                                <button
                                    className='reset'
                                    onClick={this.handleReset.bind(this, 'playerOne')}>
                                        Reset
                                </button>
                        </PlayerPreview>
                    }
                    {!playerTwoName &&
                        <PlayerInput
                            id='playerTwo'
                            label='Player Two'
                            onSubmit={this.handleSubmit}
                        />}
                    {playerTwoImage &&
                        <PlayerPreview
                            avatar={playerTwoImage}
                            username={playerTwoName}>
                                <button
                                    className='reset'
                                    onClick={this.handleReset.bind(this, 'playerTwo')}>
                                        Reset
                                </button>
                        </PlayerPreview>
                    }
                </div>
                {playerOneImage && playerTwoImage &&
                  <Link className='button' to={{
                      pathname: this.props.match.url + '/results',
                      search: '?playerOneName=' + playerOneName + '&playerTwoName=' + playerTwoName
                  }}>
                      Battle
                  </Link>
                }
            </div>
        )
    }
}