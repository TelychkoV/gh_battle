import React, {memo} from 'react';

function SelectedLanguages(props) {
    const languages = ['All', 'Javascript', 'Ruby', 'Java', 'CSS', 'Python'];
    return (
        <ul className='languages'>
            {languages.map((language, index) =>
                    <li
                        onClick={language !== props.selectedLanguage ? () => props.onSelect(language) : null}
                        style={language === props.selectedLanguage ? { color: '#d0021b'} : null}
                        key={index}>
                            {language}
                    </li>
                )
            }
        </ul>
    )
}

export default memo(SelectedLanguages);