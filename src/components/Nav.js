import React from 'react';
import {NavLink} from "react-router-dom";

export default function Nav() {
    const navLinks = ['Home', 'Popular', 'Battle'];
    return (
        <ul className='nav'>
            {navLinks.map((link, index) =>
                <li key={index}>
                    <NavLink
                        exact
                        activeClassName='active'
                        to={link === 'Home' ? '/' : link.toLowerCase()}>
                            {link}
                    </NavLink>
                </li>

            )}
        </ul>
    )
}