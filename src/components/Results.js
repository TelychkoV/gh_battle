import React, {Component, Fragment} from 'react';
import qs from 'query-string';
import {battle} from "../utils/api";
import Player from "./Player";
import {Link} from "react-router-dom";

export default class Results extends Component {
    constructor(props) {
        super(props);
        this.state = {
            winner: null,
            loser: null,
            loading: true,
            error: null
        }
    }

    componentDidMount() {
        const players = qs.parse(this.props.location.search);
        battle([
            players.playerOneName,
            players.playerTwoName
        ]).then(players => {
            if(!players) {
                this.setState({ error: 'Looks like there was an error. Check both users exist!', loading: false})
            }

            this.setState({
                winner: players[0],
                loser: players[1],
                loading: false,
                error: null
            })

        })
    }

    render() {
        const {winner, loser, loading, error} = this.state;

        if(loading) {
            return <p style={{textAlign: 'center'}}>Loading ...</p>
        }

        if(error) {
           return (
               <Fragment>
                   <p>{error}</p>
                   <Link to='/battle' />
               </Fragment>
           )
        }

        return (
            <div className="row">
                <Player
                    label='Winner'
                    score={winner.score}
                    profile={winner.profile}
                />
                <Player
                    label='Loser'
                    score={loser.score}
                    profile={loser.profile}
                />
            </div>
        )
    }
}