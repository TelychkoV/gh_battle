import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import Home from "./Home";
import Popular from "./Popular";
import Battle from "./Battle";
import Results from "./Results";
import Nav from "./Nav";

export default function App() {
    const routes = [
        {path: '/', component: Home},
        {path: '/popular', component: Popular},
        {path: '/battle', component: Battle},
        {path: '/battle/results', component: Results}
    ];

    return (
        <Router>
            <div className='container'>
                <Nav />
                <Switch>
                    {routes.map((route, index) =>
                        <Route
                            key={index}
                            exact
                            path={route.path}
                            component={route.component}
                        />
                    )}
                    <Route render={() => <p>Page Not Found</p>} />
                </Switch>
            </div>
        </Router>
    )
}